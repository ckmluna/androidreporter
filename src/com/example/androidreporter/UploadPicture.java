package com.example.androidreporter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UploadPicture extends ActionBarActivity implements View.OnClickListener{
	public EditText title, desc;
	public Button save, choose,icon1, icon2, icon3;
	public TextView selected, fill;
	String imgcode = "icon1";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_upload_picture);
		
		icon1 = (Button) findViewById(R.id.img1);
		icon1.setClickable(true);
		icon2 = (Button) findViewById(R.id.img2);
		icon2.setClickable(true);
		icon3 = (Button) findViewById(R.id.img3);
		icon3.setClickable(true);
		save = (Button) findViewById(R.id.save);
		
		icon1.setOnClickListener(this);
		icon2.setOnClickListener(this);
		icon3.setOnClickListener(this);
		save.setOnClickListener(this);
		
		selected = (TextView) findViewById(R.id.selected);
		selected.setVisibility(View.GONE);
		
		title = (EditText) findViewById(R.id.title);
		desc = (EditText) findViewById(R.id.desc);
		fill = (TextView) findViewById(R.id.fillup);
		fill.setVisibility(View.GONE);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		EditText newTitle = (EditText) findViewById (R.id.newTitle);
		EditText newDes = (EditText) findViewById (R.id.newDesc);
		
		int newIMG = 1;
		
		Log.d("ONCLICK", "ICON");
		
		switch(v.getId()) {
        case R.id.img1:
        	imgcode = v.getContentDescription().toString();
            break;
        case R.id.img2:
        	imgcode = v.getContentDescription().toString();
            break;
        case R.id.img3:
        	imgcode = v.getContentDescription().toString();  
            break;
		}

		selected.setVisibility(View.VISIBLE);
		selected.setText("Selected: "+imgcode);
		
		if(v.getId() == R.id.save){
			if(newTitle.getText() == null)
				Log.d("NULL", "TITLE");
			Log.d("title value", newTitle.getText().toString());
			
			if(newTitle.getText().toString().equals("") || newDes.getText().toString().equals(""))
				fill.setVisibility(View.VISIBLE);
			else{
				Log.d("SAVE", "REPORT FOR SAVE");
				String mDrawableName = "@drawable/"+imgcode;
				newIMG = getResources().getIdentifier(mDrawableName , "drawable", getPackageName());
				Intent addReport = getIntent();
				
				addReport.putExtra("TITLE", newTitle.getText().toString());
				addReport.putExtra("DESC", newDes.getText().toString());
				addReport.putExtra("IMG", newIMG);
				setResult(RESULT_OK, addReport);
				
				finish();	
			}
		}	
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.upload_picture, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_upload_picture,
					container, false);
			return rootView;
		}
	}

}
