package com.example.androidreporter;

import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	
	HashMap<String, String> users;
	EditText username, password;
	Button login;
	TextView invalid;
	public static String EXTRA_UNAME = "com.example.MainActivity.UNAME";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
		
		users = new HashMap<String, String>();
		
		users.put("CK", "CK");
		users.put("Bey", "Bey");
		users.put("Jean", "Jean");
		users.put("Engel", "Engel");
		users.put("Errol", "Errol");
		
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		login = (Button) findViewById(R.id.login);
		invalid = (TextView) findViewById(R.id.invalid);
		
		invalid.setVisibility(View.GONE);
		
		login.setOnClickListener(new OnClickListener() {
			//Onclick
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String enteredName = username.getText().toString();
				String enteredPassword = password.getText().toString();
				
				//Log.d(enteredPassword, users.get(enteredName).toString());
				
				if((enteredPassword != null || enteredName != null)
					&& enteredPassword.equals(users.get(enteredName))){
					Log.d("LOGIN", "LOGIN");
					Intent i = new Intent(MainActivity.this, RecordList.class);
					i.putExtra(EXTRA_UNAME, enteredName);
					startActivity(i);
					finish();
				}
				else
					invalid.setVisibility(View.VISIBLE);
			}
		});

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
		
		//EOWZ POHKAHSALS 222
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
