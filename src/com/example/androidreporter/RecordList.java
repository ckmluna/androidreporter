package com.example.androidreporter;


import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class RecordList extends ActionBarActivity{
	 	private DrawerLayout mDrawerLayout;
	    private ListView mDrawerList;
	    private ActionBarDrawerToggle mDrawerToggle;

	    private String[] mMenuTitles;
	    int position;

		int EDIT_OK = 123;
		int DELETE_REPORT = 100;
		int VIEW_REPORT = 12;
		public HashMap<String, ListPair> records;
		ListView listview;
		Button addReport;
		public static int ADD_REPORT = 500;
		
		public class ListPair{
			String desc;
			int img;
			public ListPair(String desc, int img){
				this.desc = desc;
				this.img = img;
			}
		}
	@Override

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_record_list);

        mMenuTitles = getResources().getStringArray(R.array.menu_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mMenuTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        
        
        Log.d("POSITION", "pos :" + position);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                 
                mDrawerLayout,         
                R.drawable.ic_drawer, 
                R.string.drawer_open, 
                R.string.drawer_close 
                ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();             }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();             }
        };
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
		
		records = new HashMap<String, ListPair>();
		records.put("clear()", new ListPair("Removes all of the mappings from this map.", R.drawable.icon1));
		records.put("clone()", new ListPair("Returns a shallow copy of this HashMap instance: the keys and values themselves are not cloned.", R.drawable.icon2));
		records.put("containsKey(Object key)", new ListPair("Returns true if this map contains a mapping for the specified key", R.drawable.icon3));
	}
	
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		List<HashMap<String,String>> reports = new ArrayList<HashMap<String,String>>();
		
		for (String key : records.keySet()) {
			HashMap<String, String> hm = new HashMap<String,String>();
			hm.put("title", key);
			
			ListPair details = (ListPair) records.get(key);
			
			hm.put("img", Integer.toString(details.img));
			hm.put("desc", details.desc);
			
			reports.add(hm);
		}

		String[] source = {"title", "img", "desc"};
		int[] dest = {R.id.title, R.id.img, R.id.desc};
		
		SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), reports, R.layout.list_view_wit_image, source, dest);
		
		listview = (ListView) findViewById(R.id.recordsList);
		listview.setAdapter(adapter);
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent open = new Intent(RecordList.this, EditRecord.class);
				
				HashMap<String, String> clicked = (HashMap<String, String>) listview.getItemAtPosition(position);
				
				for(String key : clicked.keySet()){
					String det = clicked.get(key);
					Log.d("TEST", clicked.get("title"));	
					open.putExtra("reportDesc", clicked.get("desc"));
					open.putExtra("imgName", getResources().getResourceEntryName(Integer.parseInt(clicked.get("img"))));
					open.putExtra("title", clicked.get("title"));
					Log.d("DETAILS", " " + getResources().getResourceEntryName(Integer.parseInt(clicked.get("img"))));
					
				}
				startActivityForResult(open, VIEW_REPORT);
			}
		});
		
		
	}
	
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          selectItem(position);
          
          if(position == 0){
        	  
          }else if(position == 1){
        	  Intent i = new Intent(RecordList.this, UploadPicture.class);
        	  startActivityForResult(i, ADD_REPORT);
          }else if(position == 2){
             finish();
          }
        }
    }
    

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == ADD_REPORT && resultCode == RESULT_OK){ 
			
			String newTitle = data.getStringExtra("TITLE"),
					newDesc = data.getStringExtra("DESC");
			int newImg = data.getIntExtra("IMG", 1);
			
			records.put(newTitle, new ListPair(newDesc, newImg));
			
		}
		if(requestCode == VIEW_REPORT && resultCode == EDIT_OK){
			// TODO update report
			String title = data.getStringExtra("title");
			String desc = data.getStringExtra("desc");
			
			//image name unchanged 
			int img_name = data.getIntExtra("img",1);
			records.remove(title);
			records.put(title, new ListPair(desc, img_name));
		}
		if(requestCode == VIEW_REPORT && resultCode == DELETE_REPORT){
			// TODO delete report using title
			String title = data.getStringExtra("title");
			records.remove(title);
		}
	}

    private void selectItem(int position) {
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
        
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
