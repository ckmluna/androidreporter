package com.example.androidreporter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class EditRecord extends ActionBarActivity {
	String reportDesc;
	String imgName;
	String title;
	TextView titleHolder;
	ImageView reportImg;
	String imgUri;
	EditText reportDesc_field;
	int EDIT_OK = 123;
	int DELETE_REPORT = 100;
	Intent returnIntent;
	Drawable res;
	int imageResource;
	
	public class ListPair{
		String desc;
		int img;
		public ListPair(String desc, int img){
			this.desc = desc;
			this.img = img;
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_record);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	@Override
	protected void onResume() {
		Intent intent = getIntent();    
		//HashMap<String, ListPair> hashMap = (HashMap<String, ListPair>) intent.getSerializableExtra("hashMap");
		reportDesc = intent.getStringExtra("reportDesc");
		imgName = intent.getStringExtra("imgName");
		title = intent.getStringExtra("title");
		//reportDesc = hashMap.get("desc").toString();
		//imgName = hashMap.get("img").toString();
		//title = hashMap.get("title").toString();

		
		Log.d("DEBUG", "Decription: " + reportDesc);
		
		titleHolder = (TextView)findViewById(R.id.reportTitle);
		titleHolder.setText(title);
		
		reportImg = (ImageView)findViewById(R.id.reportImg);
		Log.d("imgName",imgName);
		imgUri = "@drawable/"+imgName;
		imageResource = getResources().getIdentifier(imgUri, null, getPackageName());
		res = getResources().getDrawable(imageResource);
		
		reportImg.setImageDrawable(res);
		
		reportDesc_field = (EditText)findViewById(R.id.reportDesc);
		reportDesc_field.setText(reportDesc);
		
		super.onResume();
	}
	
	public void SaveChanges(View v){
		Log.d("SaveChanges","Saving...");
		returnIntent = new Intent();
		returnIntent.putExtra("title", title);
		returnIntent.putExtra("desc", reportDesc_field.getText().toString());
		returnIntent.putExtra("img", imageResource);
		setResult(EDIT_OK, returnIntent);
		finish();
	}
	public void DeleteReport(View v){
		Log.d("DeleteReport","Delete");
		returnIntent = new Intent();
		returnIntent.putExtra("title", title);
		setResult(DELETE_REPORT, returnIntent);
		finish();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_record, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_edit_record,
					container, false);
			return rootView;
		}
	}

}
